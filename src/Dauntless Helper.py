#!/usr/bin/env python

# tk needs relative paths

__author__ = "Damian Komorowski"
__copyright__ = "Copyright 2021, Threef"
__credits__ = []
__version__ = "0.0.3"
__license__ = "GPL"
__email__ = "midan666@gmail.com"
__status__ = "Development"

import os
import time
import math
import numpy as nm
import pytesseract
import re
import cv2
import csv
from unidecode import unidecode
from win32gui import GetWindowText, GetForegroundWindow
from PIL import Image

if not os.path.exists('learned data'):
    os.makedirs('learned data')

from PIL import ImageGrab

pytesseract.pytesseract.tesseract_cmd = 'D:/Program Files (x86)/Tesseract-OCR/tesseract.exe'
gameTitle = "Dauntless  "

weapon = "None"
weaponLevel = 0
prestigeLevel = 0

print("Starting")


def init():
    print("Change window do Dauntless")
    success = False
    while not success:
        time.sleep(5)
        windowTitle = GetWindowText(GetForegroundWindow())
        if windowTitle == gameTitle:
            success = True
            print("Game window found")


def mse(imageA, imageB):
    # the 'Mean Squared Error' between the two images is the
    # sum of the squared difference between the two images;
    # NOTE: the two images must have the same dimension
    err = nm.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
    err /= float(imageA.shape[0] * imageA.shape[1])

    # return the MSE, the lower the error, the more "similar"
    # the two images are
    return err


def isInCombat():
    return readWeapon() != "None"  # TK find better way


def isInHunt():
    return readWeapon() != "None"


def readBehemoth():
    windowTitle = GetWindowText(GetForegroundWindow())
    if windowTitle == gameTitle and isInCombat():
        date_string = str(math.floor(time.time()))
        cap = ImageGrab.grab(bbox=(1750, 90, 1850, 170))
        img2 = nm.array(cap.convert('RGB'))

        # see if you already have that behe
        r = "None"
        bestGuess = 1000
        for behe in os.listdir("icons/Behemoths/"):
            if not "__" in behe:
                img = cv2.imread("icons/Behemoths/" + behe)
                # diff_img = cv2.subtract(img,img2)
                # w,h,c=diff_img.shape
                # total_pixel_value_count=w*h*c*255
                # percentage_match=(total_pixel_value_count -nm.sum(diff_img))/total_pixel_value_count*100
                # print(percentage_match, behe, diff_img)
                # if percentage_match >= 95 and percentage_match >= bestGuess:
                #     bestGuess = percentage_match
                #     r = re.sub(r'.png$', '', behe)

                percentage_match = mse(cv2.cvtColor(
                    img, cv2.COLOR_BGR2GRAY), cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY))
                if percentage_match < bestGuess:
                    bestGuess = percentage_match
                    r = re.sub(r'.png$', '', behe)

        print("you're fighting", r)

        if r == "None":
            print("new behe?")
            cap.save("icons/Behemoths/__" + date_string + ".png", "PNG")
        return r


def readWeapon():
    windowTitle = GetWindowText(GetForegroundWindow())
    if windowTitle == gameTitle:
        date_string = str(math.floor(time.time()))
        cap = ImageGrab.grab(bbox=(140, 0, 185, 56))
        img2 = nm.array(cap.convert('RGB'))
        #cap.save( "weapon" + date_string +".png", "PNG" )

        w = "None"
        weaponList = ["Axe", "Chainblades", "Hammer",
                      "Repeaters", "Strikers", "Sword", "Warpike"]
        bestGuess = 10000
        for i in weaponList:
            img = cv2.imread("icons/" + i + ".png")

            percentage_match = mse(cv2.cvtColor(
                img, cv2.COLOR_BGR2GRAY), cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY))
            # diff_img = cv2.subtract( img, img2 )
            # w,h,c=diff_img.shape
            # total_pixel_value_count=w*h*c*255
            # percentage_match=(total_pixel_value_count -nm.sum(diff_img))/total_pixel_value_count*100

            if percentage_match < bestGuess:
                w = i
                bestGuess = percentage_match

        return w


def readWeaponLevel():
    global weaponLevel
    windowTitle = GetWindowText(GetForegroundWindow())
    if windowTitle == gameTitle:
        cap = ImageGrab.grab(bbox=(789, 1005, 822, 1030))
        grayscale = cv2.cvtColor(nm.array(cap), cv2.COLOR_BGR2GRAY)
        tesstr = pytesseract.image_to_string(
            grayscale, config='--psm 7 --oem 3 -c tessedit_char_whitelist=0123456789')
        tesstr = unidecode(tesstr)
        tesstr = re.sub(r'[^0-9]', '', tesstr)
        if type(tesstr) is str and len(tesstr) > 0:
            val = int(tesstr)
            if val > 0 and val <= 20:
                if(val != weaponLevel):
                    weaponLevel = val
    return weaponLevel


def readPrestigeLevel():
    windowTitle = GetWindowText(GetForegroundWindow())
    if windowTitle == gameTitle:
        cap = ImageGrab.grab(bbox=(798, 1031, 815, 1045))
        grayscale = cv2.cvtColor(nm.array(cap), cv2.COLOR_BGR2GRAY)
        tesstr = pytesseract.image_to_string(
            grayscale, config='--psm 7 --oem 3 -c tessedit_char_whitelist=0123456789/,')
        tesstr = unidecode(tesstr)
        tesstr = re.sub(r'[^0-9]', '', tesstr)
        if type(tesstr) is str and len(tesstr) > 0:
            print(tesstr)
            val = int(tesstr)
            if val > 0:
                # if( val != prestigeLevel ):
                #     if weapon != readWeapon():

                return val


def readPrestigeXp():
    global weapon
    global weaponLevel
    global prestigeLevel
    # font used is Roboto Condensed Regular 400
    windowTitle = GetWindowText(GetForegroundWindow())
    if windowTitle == gameTitle:
        weapon = readWeapon()
        weaponLevel = readWeaponLevel()
        prestigeLevel = readPrestigeLevel()

        cap = ImageGrab.grab(bbox=(870, 1007, 1050, 1032))
        grayscale = cv2.cvtColor(nm.array(cap), cv2.COLOR_BGR2GRAY)
        tesstr = pytesseract.image_to_string(
            grayscale, config='--psm 7 --oem 3 -c tessedit_char_whitelist=0123456789/,')
        tesstr = unidecode(tesstr)
        tesstr = re.sub(r'[^0-9/]', '', tesstr)
        r = re.search(r"([0-9]+)/([0-9]+)", tesstr)
        if r:
            xp = int(r.group(1))
            xpNeed = int(r.group(2))
            if xp <= xpNeed and xp < 2000 and xpNeed < 2000:
                # if valid values for XP
                print(xp, xpNeed)

                date_string = str(math.floor(time.time()))
                with open('PrestigeXP.csv', mode='a') as csv_file:
                    writer = csv.writer(
                        csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                    writer.writerow(
                        [date_string, weapon, weaponLevel, prestigeLevel, xp, xpNeed])


def testLearnedData():
    filelist = [file for file in os.listdir(
        'learned data') if file.endswith('.png')]
    for i in filelist:
        #print( i )
        cap = Image.open("learned data/"+i)
        grayscale = cv2.cvtColor(nm.array(cap), cv2.COLOR_BGR2GRAY)
        tesstr = pytesseract.image_to_string(
            grayscale, config='--psm 7 --oem 3 -c tessedit_char_whitelist=0123456789/,')
        tesstr = unidecode(tesstr)
        tesstr = re.sub(r'[^0-9/]', '', tesstr)
        r = re.search(r"([0-9]+)/([0-9]+)", tesstr)
        if r:
            xp = int(r.group(1))
            xpNeed = int(r.group(2))
            if(xp <= xpNeed):
                print(xp, xpNeed)

        #date_string = str( math.floor( time.time() ) )
        # with open('PrestigeXP.csv', mode='a') as csv_file:
            #employee_writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            #employee_writer.writerow([date_string, tesstr])


def learnScreenshots():
    windowTitle = GetWindowText(GetForegroundWindow())
    date_string = str(math.floor(time.time()))
    cap = ImageGrab.grab(bbox=(870, 1007, 1050, 1032))
    if windowTitle == gameTitle:
        cap.save("learned data/color" + date_string + ".png", "PNG")


# testLearnedData()
init()

while True:
    # learnScreenshots()
    readPrestigeXp()
    readBehemoth()
    time.sleep(10)
