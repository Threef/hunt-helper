=================
Hunt Helper
=================
Hunt Helper is a Python application that collects information by scanning game screen. So far it doesn't do anything other than saving stats to csv file.

Getting Started
---------------

-------------
Prerequisites
-------------
- Python X.X with pip
- Dauntless

------------
Installation
------------
``pip install -r requirements.txt``